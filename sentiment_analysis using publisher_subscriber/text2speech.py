#! /usr/bin/env python

# Copyright <2018> <NITHIN THOMAS>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from gtts import gTTS
import os 

def text(result):#functin for speech output specifying sentiment.
    	tts = gTTS(text=result, lang='en')#value of result is passed to text and english language is set
    	tts.save("good.mp3")#mp3 file named good.mp3 is saved
    	os.system("mpg321 good.mp3")

def callback(data):#a call_back_function is defined
    rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    a=data.data#copying contents in data.data to a
    b=a.split()#splitts sentence into words
    list1=['good','happy']#list_1 showing good sentiment
    list2=['bad','sad']#list_2 for bad sentiments
    count=0
    c=0
    #starting iterations to check the sentiment analysis
    for i in range (0,len(b)):
	for j in range (0,2):
        	if b[i]==list1[j]:
			count+=1
	for k in range (0,2):
		if b[i]==list2[k]:
			c+=1
    #checking for conditions and comparing results 
    if count>c:
	result="you sounded positive"
        print "you sounded positive"
	text(result)
    elif count==c:
	result="you sounded neutral"
        print "you sounded neutral"
	text(result)
    else:
	result="you sounded negative"
        print "you sounded negative"
	text(result)
			
  
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("chatter", String, callback)
    rospy.spin()# spin() simply keeps python from exiting until this node is stopped


if __name__ == '__main__':
    listener()








