#! /usr/bin/env python

# Copyright <2018> <NITHIN THOMAS>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/usr/bin/env python
# license removed for brevity
import speech_recognition as sr #importing_speech_recognition_api
import rospy
from std_msgs.msg import String
#import speech_recognition as sr
def talker():
        pub = rospy.Publisher('chatter', String, queue_size=10)
        rospy.init_node('talker', anonymous=True)# anonymous=True flag means that rospy will choose a unique listener
        rate = rospy.Rate(10) # 10hz
        #starting while loop to take speech values until a real pause from author 
        while not rospy.is_shutdown():
	    r = sr.Recognizer()
	    r.energy_threshold =48000
	    with sr.Microphone() as source:
    		r.adjust_for_ambient_noise(source)#function to supress noise
    		print("Say something!")
    		audio = r.listen(source)#
    		print "Recognising........."# fron this onwards in after loop;
    		try:
    			print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
        		textt=r.recognize_google(audio)#speech file is copied into textt
    		except sr.UnknownValueError:
    	 		print("Google Speech Recognition could not understand audio")
    		except sr.RequestError as e:
    	 		print("Could not request results from Google Speech Recognition service; {0}".format(e))
 
                rospy.loginfo(textt)
                pub.publish(textt)
                rate.sleep()
        

	
if __name__ == '__main__':
    try:
	talker()
    except rospy.ROSInterruptException:
        pass



