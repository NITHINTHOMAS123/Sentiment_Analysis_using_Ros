#! /usr/bin/env python

# Copyright <2018> <NITHIN THOMAS>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/usr/bin/env python
import sys
import rospy
import speech_recognition as sr
from client_server.srv import *
from gtts import gTTS
import os
from std_msgs.msg import String
def sentiment_analysis_client(x):
    rospy.wait_for_service('sentiment_analysis')
    try:
        sentiment_analysis = rospy.ServiceProxy('sentiment_analysis', SentimentAnalysis)
        resp1 = sentiment_analysis(x)
        return resp1.sentiment
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

def usage():
    return "%s [x]"%sys.argv[0]

if __name__ == "__main__":
    r = sr.Recognizer()
    r.energy_threshold =48000
    with sr.Microphone() as source:
    	r.adjust_for_ambient_noise(source)
    	print("Say something!")
    	audio = r.listen(source)
    	print "Recognising........."
    try:
    	print("Google Speech Recognition thinks you said " + r.recognize_google(audio))
        text=r.recognize_google(audio)
    except sr.UnknownValueError:
    	 print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
    	 print("Could not request results from Google Speech Recognition service; {0}".format(e))
    print "Requesting Server........"
    textt=sentiment_analysis_client(text)
    print textt
    tts = gTTS(text=textt, lang='en')
    tts.save("good.mp3")
    os.system("mpg321 good.mp3")

        
    
