#! /usr/bin/env python

# Copyright <2018> <NITHIN THOMAS>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#!/usr/bin/env python
from client_server.srv import *
import rospy
from gtts import gTTS
import os

def handle_sentiment_analysis(req):
    text=req.a
    a=text.split()
    list1=['good','happy']
    list2=['bad','sad']
    count=0
    c=0
    for i in range (0,len(a)):
	for j in range (0,2):
        	if a[i]==list1[j]:
			count+=1
	for k in range (0,2):
		if a[i]==list2[k]:
			c+=1
    if count>c:
	result="you sounded positive"
	return SentimentAnalysisResponse(result)
    elif count==c:
	result="you sounded neutral"
	return SentimentAnalysisResponse(result)
    else:
	result="you sounded negative"
	return SentimentAnalysisResponse(result)

def sentiment_analysis_server():
    rospy.init_node('sentiment_analysis_server')
    result = rospy.Service('sentiment_analysis', SentimentAnalysis, handle_sentiment_analysis)
    rospy.spin()

if __name__ == "__main__":
    sentiment_analysis_server()
