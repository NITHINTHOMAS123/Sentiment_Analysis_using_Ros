#! /usr/bin/env python

#Copyright <2018> <NITHIN THOMAS>

#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import rospy
import actionlib
import sentiment_analysis_action_server.msg

class SentimentAnalysis(object):
    # create messages that are used to publish feedback/result
    _feedback = sentiment_analysis_action_server.msg.SpeechFeedback()
    _result = sentiment_analysis_action_server.msg.SpeechResult()
   
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, sentiment_analysis_action_server.msg.SpeechAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        # publish info to the console for the user
        rospy.loginfo('performing sentiment analysis of %s'%goal.text)
        a=goal.text
	a=a.split()
        # start executing the action
	count=0
        c=0
        c1=0
	neutral=['hello','hii']
	positive=['good','happy']
	negative=['sad','bad']
	for k in range (0,len(a)):
		# check that preempt has not been requested by the client
		if self._as.is_preempt_requested():
			rospy.loginfo('%s: Preempted' % self._action_name)
                	self._as.set_preempted()
                	success = False
                	break
		# Comparing text with lists
		if a[k] in neutral:
			c1=c1+1
        	if a[k] in negative:
                	c=c+1
        	if a[k] in positive:
			count=count+1
	# Sentiment analysis
	if (count > c) and (count > c1):
        	self._feedback.sequence="sentence is positive"
		self._as.publish_feedback(self._feedback)	# publish the feedback
		r.sleep()				        # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes	
	elif (c1 > count) and (c1 > c):
        	self._feedback.sequence="sentence is neutral"
		self._as.publish_feedback(self._feedback)
		r.sleep()
	elif (c >= count) and (c > c1):
		self._feedback.sequence="sentence is negative"
		self._as.publish_feedback(self._feedback)
		r.sleep()
	else:
        	self._feedback.sequence="invalid"
		self._as.publish_feedback(self._feedback)
		r.sleep()
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('speech')
    server = SentimentAnalysis(rospy.get_name())
    rospy.spin()
