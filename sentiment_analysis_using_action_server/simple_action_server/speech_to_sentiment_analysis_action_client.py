#! /usr/bin/env python

# Copyright <2018> <NITHIN THOMAS>

# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function
import rospy
# Brings in the SimpleActionClient
import actionlib
# Brings in the messages used by the fibonacci action, including the
# goal message and the result message.
import sentiment_analysis_action_server.msg
import sys
import os
import speech_recognition as sr
from speech.srv import *
from std_msgs.msg import String
from gtts import gTTS

def sentiment_client():
    # Creates the SimpleActionClient, passing the type of the action
    # (FibonacciAction) to the constructor.
    client = actionlib.SimpleActionClient('speech', sentiment_analysis_action_server.msg.SpeechAction)

    # Waits until the action server has started up and started
    client.wait_for_server()
    # listening for goals.
    # takes input from microphone and converts it to text
    r=sr.Recognizer()
    r.energy_threshold =48000
    with sr.Microphone() as source:
        r.adjust_for_ambient_noise(source)
        print ('say something')
        audio =r.listen(source)
        print ("done")
   	a=r.recognize_google(audio)

    # Creates a goal to send to the action server.
    goal = sentiment_analysis_action_server.msg.SpeechGoal(text=a)

    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()	# Sentiment analysis Result

if __name__ == '__main__':
    try:
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('speech_client_py')
        result = sentiment_client()
	print("Result:",result.sequence)
	text1=result.sequence
	t=gTTS(text=text1,lang='en')
        t.save('myfile.mp3')
        os.system("mpg321 myfile.mp3")
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)

